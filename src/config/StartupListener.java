package config;

import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import model._MappingKit;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

public class StartupListener implements ApplicationListener<ContextRefreshedEvent> {

    private boolean startflag=false;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        System.out.println("run after springmvc start");
        if (!startflag){
            System.out.println("inti activeplugin");
            PropKit.use("a_little_config.txt");
            DruidPlugin druidPlugin = new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
            startflag=druidPlugin.start();
            ActiveRecordPlugin arp=new ActiveRecordPlugin(druidPlugin);
            _MappingKit.mapping(arp);
            arp.setShowSql(true);
            arp.setDevMode(true);
            if(startflag){
                startflag=arp.start();
            }
        }
    }
}
