package util;

import java.util.UUID;

public class IDUtils {

    public static String getId(){
        String uuid=UUID.randomUUID().toString().replaceAll("-","");
        return uuid;
    }
}
