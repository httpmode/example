package util;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.druid.DruidPlugin;

public class DbBeanGeneratorUtil {

    public static void main(String[] args) {
        // base model 所使用的包名
        String baseModelPkg = "model.base";
        // base model 文件保存路径
        String baseModelDir = PathKit.getWebRootPath() + "/src/model/base";

        // model 所使用的包名
        String modelPkg = "model";
        // model 文件保存路径
        String modelDir = baseModelDir + "/..";
        PropKit.use("a_little_config.txt");
        DruidPlugin druidPlugin = new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
        druidPlugin.start();
        Generator gernerator = new Generator(druidPlugin.getDataSource(), baseModelPkg, baseModelDir, modelPkg, modelDir);
        gernerator.setGenerateChainSetter(true);
        gernerator.setGenerateDaoInModel(true);
        gernerator.generate();
        druidPlugin.stop();
    }
}
