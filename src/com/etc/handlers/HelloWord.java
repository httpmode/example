package com.etc.handlers;

import model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import util.IDUtils;

@Controller
public class HelloWord {

    @RequestMapping("hello")
    public String hello(Model m,String email,String password){
        System.out.println("hello world!"+m);
        User user=new User();
        user.setId(IDUtils.getId()).setName(email).setPassword(password).save();
        m.addAttribute("user",user);
        return "index";
    }
}
